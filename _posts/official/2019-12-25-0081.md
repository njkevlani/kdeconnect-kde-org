---
layout: post
title: "KDE Connect Website- Taking it Further"
date: 2020-01-4 16:02:00
type: official
---

<p>Happy to announce that the Connect Website is almost over.</p>

<p>Many of the suggestions and feedbacks were taken into consideration and implemented. Webp support is also added for images for faster load times. The homepage has a new Video showing of the features which I am planning to make a bit more better. Will update on that soon. The video is currently on my Youtube Channel which can be moved to some other location once the development of the website is complete. </p>

<p>Hope to get more feedback on what else to change.</p>

<p class="author">— T.H.Arjun</p>
